﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;

public class LevelCheckpoint : MonoBehaviour {
    [SerializeField] private GameObject levelSummary;
    [SerializeField] private LevelsData levelData;
    [SerializeField] private TextMeshProUGUI scoreValue;
    [SerializeField] private TextMeshProUGUI timeCountValue;
    [SerializeField] List<Image> healthList;

    private Health playerHealth;

    private void Start() {
        playerHealth = FindObjectOfType<PlayerController>().myHealth;
    }

    public void NextScene(){
        var nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;

        if (nextSceneIndex == SceneManager.sceneCountInBuildSettings){
            return;
        }

        Time.timeScale = 1;
        SceneManager.LoadScene(nextSceneIndex);
    }

    private void OnDisable()
    {
        Time.timeScale = 1;
        Score.ResetScore();
    }

    private void OnTriggerEnter2D(Collider2D other){
        if (other.gameObject.layer != LayerMask.NameToLayer("Player")){
            return;
        }
        var currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        levelData.incrementLevel(currentSceneIndex);
        
        Time.timeScale = 0;
        levelSummary.SetActive(true);

        int receivedScore = Score.getScore();
        scoreValue.text = receivedScore.ToString();
        timeCountValue.text = Timer.currentTime;
  
        if (levelData.getScoreOnLevel(currentSceneIndex - 1) <= receivedScore) {
            levelData.setScoreOnLevel(currentSceneIndex - 1, receivedScore);
        }

        if (levelData.getTimeOnLevel(currentSceneIndex - 1) > Timer.time || levelData.getTimeOnLevel(currentSceneIndex - 1) == 0) {
            levelData.setTimeOnLevel(currentSceneIndex - 1, Timer.time);
        }

        for (int i = 0; i < playerHealth.CurrentPoints; i++) {
            healthList[i].gameObject.SetActive(true);
        }
    }
}