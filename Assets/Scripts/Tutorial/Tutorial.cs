﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _textMeshPro;
    [SerializeField] private SpriteRenderer _spriteRenderer;

    [SerializeField] private Transform playerObject;
    [SerializeField] private float minDistance = 5f;

    [SerializeField] private float maxAlpha_Text = 1f;
    [SerializeField] private float maxAlpha_Sprite = 0.6f;
    
    private void Update()
    {
        var textColor = _textMeshPro.color;
        textColor.a = Mathf.Lerp(maxAlpha_Text, 0f, Vector3.Distance(transform.position, playerObject.position) / minDistance);
        _textMeshPro.color = textColor;

        var spriteColor = _spriteRenderer.color;
        spriteColor.a = Mathf.Lerp(maxAlpha_Sprite, 0f, Vector3.Distance(transform.position, playerObject.position) / minDistance);
        _spriteRenderer.color = spriteColor;
    }
}
