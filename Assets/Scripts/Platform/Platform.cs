﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(Disappear))]
public class Platform : MonoBehaviour{
    [SerializeField] private Vector2 offset;
    [SerializeField] private float moveTime;
    [SerializeField] private bool looping;
    [SerializeField] private float platformComebackDelay;
    [SerializeField] public bool disappearOnlyOnCollision;
    [SerializeField] private bool startMovingOnCollision;
    [SerializeField] private bool stopMovingOnExit;

    private Vector2 _positionDelta;
    private Vector2 _initialPosition;
    private Vector2 _finalPosition;
    private Vector2 _currentPosition;
    private List<GameObject> _objects;
    private Transform _platformTransform;
    private Disappear _disappear;
    private Coroutine _movingCoroutine;
    private float time;

    private void Awake(){
        _platformTransform = GetComponent<Transform>();
        _objects = new List<GameObject>();
        _disappear = GetComponent<Disappear>();
        if (disappearOnlyOnCollision) _disappear.enabled = false;
    }

    void Start(){
        _initialPosition = _platformTransform.position;
        _finalPosition = _initialPosition + offset;
        if (!startMovingOnCollision){
            _movingCoroutine = StartCoroutine(MovingCoroutine());
        }
    }
    
    private void OnCollisionEnter2D(Collision2D other){
        _objects.Add(other.gameObject);
        DisappearOnCollision();
        StartMovingOnCollision();
    }

    private void OnCollisionExit2D(Collision2D other){
        if (_objects.Count != 0) _objects.Remove(other.gameObject);
        StopMovingOnExit();
    }

    private void StopMovingOnExit(){
        if (stopMovingOnExit){
            StopCoroutine(_movingCoroutine);
        }
    }

    private void DisappearOnCollision(){
        if (_disappear != null && _disappear.isDisappearing){
            _disappear.enabled = true;
            _disappear.DisappearAndAppearCoroutine();
        }
    }

    private void StartMovingOnCollision(){
        if (!startMovingOnCollision) return;
        _movingCoroutine = StartCoroutine(MovingCoroutine());
    }

    private void MoveCollidingObjects(Vector3 platformPosWhenCollided){
        _positionDelta = _platformTransform.position - platformPosWhenCollided;
        if (_objects.Count == 0) return;
        foreach (var platformObject in _objects){
            platformObject.transform.position += new Vector3(_positionDelta.x, _positionDelta.y, 0f);
        }
    }

    private IEnumerator MovingCoroutine(){
        do{
            while (time < moveTime){
                time += Time.deltaTime;
                var displacement = time / moveTime;
                var platformPosWhenCollided = _platformTransform.position;
                _platformTransform.position = Vector2.Lerp(_initialPosition, _finalPosition, displacement);
                MoveCollidingObjects(platformPosWhenCollided);
                yield return new WaitForFixedUpdate();
            }
            yield return new WaitForSeconds(platformComebackDelay);
            time = 0f;
            var tempPosition = _initialPosition;
            _initialPosition = _finalPosition;
            _finalPosition = tempPosition;
        } while (looping);
    }
}