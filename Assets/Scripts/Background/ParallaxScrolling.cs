﻿using UnityEngine;

public class ParallaxScrolling : MonoBehaviour{
    [SerializeField] private GameObject correspondingGameObject;
    [SerializeField] private float backgroundScrollingSpeedX;
    [SerializeField] private float backgroundScrollingSpeedY;
    private RectTransform _rectTransform;
    private Transform _transform;
    private Vector3 _initalPos;
    private Vector3 _deltaPos;
    private Vector3 _parallaxInitialPos;
    
    // Start is called before the first frame update
    void Start(){
        _initalPos = correspondingGameObject.transform.position;
        _parallaxInitialPos = _rectTransform.anchoredPosition;
    }

    private void Awake(){
        _rectTransform = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    private void LateUpdate(){
        _deltaPos = _initalPos - correspondingGameObject.transform.position;
        var nextPosition = new Vector2(_parallaxInitialPos.x + _deltaPos.x * backgroundScrollingSpeedX, _parallaxInitialPos.y + _deltaPos.y * backgroundScrollingSpeedY);
        _rectTransform.anchoredPosition = nextPosition;
    }

    public Vector3 GetParallaxInitialPos() {
        return _parallaxInitialPos;
    }
    
    public void SetParallaxInitialPos(Vector3 newPos) {
        _parallaxInitialPos = newPos;
    }
}