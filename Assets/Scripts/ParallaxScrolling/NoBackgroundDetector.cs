﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class NoBackgroundDetector : MonoBehaviour
{
    public LayerMask layerToDetect;

    private RaycastHit leftSideHit;
    private RaycastHit rightSideHit;

    public UnityAction<bool> CallForSwap;

    private bool called_left;
    private bool called_right;

    private void Update()
    {
        Physics.Raycast(Camera.main.ViewportPointToRay(new Vector3(0f, 0.2f, 0f)), out leftSideHit, Mathf.Infinity, layerToDetect);
        Physics.Raycast(Camera.main.ViewportPointToRay(new Vector3(1f, 0.2f, 0f)), out rightSideHit, Mathf.Infinity, layerToDetect);
        
        //LeftSideHit
        if (leftSideHit.collider != null)
        {
            called_left = false;
        }
        else
        {
            if (!called_left)
            {
                Debug.Log("Lewo");
                CallForSwap.Invoke(true);
                called_left = true;
            }
        }

        //RightSideHit
        if (rightSideHit.collider != null)
        {
            called_right = false;
        }
        else
        {
            if (!called_right)
            {
                Debug.Log("Prawo");
                CallForSwap.Invoke(false);
                called_right = true;
            }
        }
    }
}
