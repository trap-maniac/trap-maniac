﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatingBackground : MonoBehaviour
{
    [SerializeField]
    private NoBackgroundDetector myDetector;
    [SerializeField]
    private List<RectTransform> _rectTransforms;
    [SerializeField] 
    private PlayerCamera _playerCamera;
    [SerializeField]
    private float xPlacementOffset;

    private void OnEnable()
    {
        myDetector.CallForSwap += SwapBackground;
    }

    private void OnDisable()
    {
        myDetector.CallForSwap -= SwapBackground;
    }

    void SwapBackground(bool isRight)
    {
        var bestX = _rectTransforms[0].GetComponent<ParallaxBackground>().GetParallaxInitialPos().x;
        var currentIndex = 0;
        foreach (var background in _rectTransforms) {
            if (isRight) {
                if (!(background.GetComponent<ParallaxBackground>().GetParallaxInitialPos().x > bestX)) continue;
                currentIndex = _rectTransforms.IndexOf(background);
                bestX = background.GetComponent<ParallaxBackground>().GetParallaxInitialPos().x;
            }
            else {
                if (!(background.GetComponent<ParallaxBackground>().GetParallaxInitialPos().x < bestX)) continue;
                currentIndex = _rectTransforms.IndexOf(background);
                bestX = background.GetComponent<ParallaxBackground>().GetParallaxInitialPos().x;
            }
        }
        if (isRight) {
            var background = _rectTransforms[currentIndex];
            var anchoredPosition = background.GetComponent<ParallaxBackground>().GetParallaxInitialPos();
            anchoredPosition = new Vector2(anchoredPosition.x - xPlacementOffset, anchoredPosition.y);
            background.GetComponent<ParallaxBackground>().SetParallaxInitialPos(anchoredPosition);
        }
        else {
            var background = _rectTransforms[currentIndex];
            var anchoredPosition = background.GetComponent<ParallaxBackground>().GetParallaxInitialPos();
            anchoredPosition = new Vector2(anchoredPosition.x + xPlacementOffset, anchoredPosition.y);
            background.GetComponent<ParallaxBackground>().SetParallaxInitialPos(anchoredPosition);
        }
    }
}
