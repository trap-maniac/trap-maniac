﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class ColorFlicker : MonoBehaviour
{
    [SerializeField] private float flickChange = 0.05f;
    [SerializeField] private int frameSkip = 20;
    
    [SerializeField] private Color defaultColor;
    [SerializeField] private Color flickColor;

    private SpriteRenderer mySpriteRenderer;
    
    public UnityEvent OnFlick;

    private void Awake()
    {
        mySpriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (Time.frameCount % frameSkip != 0)
            return;
        
        if (Random.value < flickChange)
        {
            mySpriteRenderer.color = flickColor;
            OnFlick.Invoke();
        }
        else
        {
            mySpriteRenderer.color = defaultColor;
        }
    }
}
