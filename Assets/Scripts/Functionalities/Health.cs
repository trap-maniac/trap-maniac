﻿using UnityEngine;
using UnityEngine.UIElements;

public class Health : MonoBehaviour
{
    [Header("Settings")]
    
    [Tooltip("Maximum amount of health")]
    [SerializeField] private int maxPoints;
    public int MaxPoints => maxPoints; //Property, cannot be assigned.
    public int CurrentPoints { get; private set; } = 3;
    
    public void AddPoints(int points)
    {
        CurrentPoints += points;
        if (CurrentPoints > MaxPoints)
            CurrentPoints = MaxPoints;
    }

    public void SetMaxPoints(int newMaxPoints) {
        maxPoints = newMaxPoints;
    }

    public void SubtractPoints(int points)
    {
        CurrentPoints -= points;
        if (CurrentPoints < 0)
            CurrentPoints = 0;
    }

    public void RefillPoints()
    {
        CurrentPoints = MaxPoints;
    }
}
