﻿public interface ICanBeDamaged
{
    void GetDamage(int damage);
}
