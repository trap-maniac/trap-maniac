﻿using System.Collections;
using UnityEngine;

public class Disappear : MonoBehaviour{
    [SerializeField] public bool isDisappearing;
    [SerializeField] private float disappearanceDelay;
    [SerializeField] private bool isDisappearingLooped;
    [SerializeField] private float remainDisappearedTime;

    private Collider2D _collider;
    private SpriteRenderer _spriteRenderer;
    private bool _isDisappeared;

    private void Awake(){
        _collider = GetComponent<Collider2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start(){
        StartCoroutine(DisappearAndAppearCoroutine());
    }

    private void DisappearOrAppear(){
        if (_collider == null) return;
        _collider.enabled = !_collider.enabled;
        _spriteRenderer.enabled = !_spriteRenderer.enabled;
    }
    
    public IEnumerator DisappearAndAppearCoroutine(){
        if (!isDisappearing || disappearanceDelay == 0) yield break;

        if (isDisappearingLooped){
            while (isDisappearingLooped){
                if (remainDisappearedTime != 0 && !_collider.enabled){
                    yield return new WaitForSeconds(disappearanceDelay + remainDisappearedTime);
                    DisappearOrAppear();
                }
                else{
                    yield return new WaitForSeconds(disappearanceDelay);
                    DisappearOrAppear();
                }
            }
        }
        else{
            yield return new WaitForSeconds(disappearanceDelay);
            DisappearOrAppear();
        }
    }
}