﻿using static Score;

public static class Score {
    public static int score;

    public static void AddScore(int score) {
        Score.score += score;
    }
    
    public static void ResetScore(){
        score = 0;
    }

    public static int getScore() {
        return score;
    }
}
