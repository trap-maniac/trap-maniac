﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GroundCheck : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private LayerMask groundLayers;
    [SerializeField] private List<Transform> groundCheckers;
    [SerializeField] private float maxDistanceFromGround = 0.1f;
    [SerializeField] private bool drawDebugLines;
    
    public bool isGrounded { get; private set; }
    
    public UnityAction OnGrounded;

    private void Update()
    {
        CheckGround();
    }

    private void CheckGround()
    {
        if (drawDebugLines)
        {
            for (int j = 0; j < groundCheckers.Count; j++)
            {
                Debug.DrawLine(groundCheckers[j].position, groundCheckers[j].position + (Vector3.down * maxDistanceFromGround), Color.magenta);
            }   
        }

        for (int i = 0; i < groundCheckers.Count; i++)
        {
            var rayHit = Physics2D.Raycast(groundCheckers[i].position, Vector2.down, maxDistanceFromGround, groundLayers);
            
            if (rayHit.collider != null)
            {
                if (!isGrounded)
                {
                    OnGrounded?.Invoke();
                }
                
                isGrounded = true;
                return;
            }
        }

        isGrounded = false;
    }
}
