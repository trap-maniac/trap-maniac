﻿using UnityEngine;

public class PlayerInputHandler
{
    private PlayerMovement _playerMovement;
    private float _horizontalInput;
    
    public PlayerInputHandler(PlayerMovement playerMovement)
    {
        _playerMovement = playerMovement;
    }

    public void HandleInput(){
        if (Pause.isPaused) return;
        _horizontalInput = Input.GetAxisRaw("Horizontal");
        _playerMovement.Move(_horizontalInput);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            _playerMovement.Jump();
        }
    }
}
