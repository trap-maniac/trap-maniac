﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudio : MonoBehaviour
{
    [SerializeField] private AudioSource[] footstepsAudioSources;
    [SerializeField] private AudioSource damagedAudioSource;
    [SerializeField] private AudioSource deathAudioSource;
    [SerializeField] private AudioSource groundedAudioSource;
    
    public void PlayFootstepSound(int index)
    {
        footstepsAudioSources[index].Play();
    }

    public void PlayDamagedSound()
    {
        damagedAudioSource.Play();
    }

    public void PlayDeathSound()
    {
        deathAudioSource.Play();
    }

    public void PlayGroundedSound()
    {
        groundedAudioSource.Play();
    }
}
