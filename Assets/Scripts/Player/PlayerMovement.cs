﻿using System;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Settings")]
    
    [SerializeField] private float maxHorizontalVelocity = 3f;
    [SerializeField] private float acceleration = 0.5f;
    [SerializeField] private float deceleration = 2f;
    [SerializeField] private float jumpForce = 5f;
    [Range(1, 3)]
    [SerializeField] private int maxJumpCount;

    [Header("Audio")] 
    
    [SerializeField] private AudioSource jumpAudioSource;

    public float currentVelocity_X { get; private set; }
    public int currentJumpCount { get; private set; }
    public float currentDeceleration { get; private set; }
    public float currentAcceleration { get; private set; }

    private Rigidbody2D myRigidbody;
    private Animator myAnimator;
    private GroundCheck myGroundCheck;

    private void OnEnable()
    {
        myGroundCheck.OnGrounded += ResetJumpCount;
    }

    private void OnDisable()
    {
        myGroundCheck.OnGrounded -= ResetJumpCount;
    }

    private void Awake()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        myGroundCheck = GetComponent<GroundCheck>();
    }

    private void Start()
    {
        currentJumpCount = 0;
        currentDeceleration = deceleration;
        currentAcceleration = acceleration;
    }

    private void Update()
    {
        UpdateDeceleration();
        UpdateAcceleration();
    }

    public void Move(float horizontalInput)
    {
        if (Mathf.Abs(horizontalInput) > 0f)
        {
            currentVelocity_X = Mathf.Lerp(currentVelocity_X, maxHorizontalVelocity * horizontalInput, Time.deltaTime * currentAcceleration);
            transform.localScale = new Vector3(horizontalInput, transform.localScale.y, transform.localScale.z);   
        }
        if (horizontalInput == 0f)
        {
            currentVelocity_X = Mathf.Lerp(currentVelocity_X, 0f, currentDeceleration * Time.deltaTime);
            
            if (Mathf.Abs(myRigidbody.velocity.x) > 0f)
            {
                myRigidbody.velocity = Vector2.Lerp(myRigidbody.velocity, new Vector2(0f, myRigidbody.velocity.y), Time.deltaTime * currentDeceleration);
            }
            return;
        }

        if (horizontalInput > 0f)
        {
            if (myRigidbody.velocity.x > maxHorizontalVelocity)
                currentVelocity_X = 0f;   
        }
        else
        {
            if (myRigidbody.velocity.x < -maxHorizontalVelocity)
                currentVelocity_X = 0f;   
        }

        myRigidbody.velocity += new Vector2(currentVelocity_X, 0f) * currentAcceleration * Time.deltaTime;   
    }

    public void Jump()
    {
        if (currentJumpCount < maxJumpCount)
        {
            jumpAudioSource.Play();
            myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, 0f);
            myRigidbody.AddForce(new Vector2(0f, jumpForce));
            currentJumpCount++;

            if (!myGroundCheck.isGrounded)
            {
                myAnimator.SetTrigger("DoubleJump");
            }
        }
    }
    
    private void ResetJumpCount()
    {
        currentJumpCount = 0;
    }

    private void UpdateDeceleration()
    {
        if (currentDeceleration < deceleration)
        {
            currentDeceleration = Mathf.Lerp(currentDeceleration, deceleration, Time.deltaTime);
        }
    }

    private void UpdateAcceleration()
    {
        if (currentAcceleration < acceleration)
        {
            currentAcceleration = Mathf.Lerp(currentAcceleration, acceleration, Time.deltaTime * 5f);
        }
    }

    public void ResetDeceleration()
    {
        currentDeceleration = 0f;
    }

    public void ResetAcceleration()
    {
        currentAcceleration = 0f;
    }
}
