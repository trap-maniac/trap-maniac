﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour, ICanBeDamaged
{
    public PlayerInputHandler myInputHandler { get; private set; }
    public PlayerMovement myMovement { get; private set; }
    public Animator myAnimator { get; private set; }
    public GroundCheck myGroundCheck { get; private set; }
    public Rigidbody2D MyRigidbody { get; private set; }
    public Health myHealth { get; private set; }

    public PlayerAudio myAudio { get; private set; }

    private void OnEnable()
    {
        myGroundCheck.OnGrounded += OnGrounded;
    }

    private void OnDisable()
    {
        myGroundCheck.OnGrounded -= OnGrounded;
    }

    private void Awake()
    {
        myMovement = GetComponent<PlayerMovement>();
        myAnimator = GetComponent<Animator>();
        myGroundCheck = GetComponent<GroundCheck>();
        MyRigidbody = GetComponent<Rigidbody2D>();
        myHealth = GetComponent<Health>();
        myAudio = GetComponent<PlayerAudio>();
        
        myInputHandler = new PlayerInputHandler(myMovement);
    }

    private void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex != 0)
        {
            LevelAudioManager.instance.PlayGameplayMusic();
        }
    }

    private void Update()
    {
        myInputHandler.HandleInput();
        UpdatePlayerAnimations();
    }

    private void UpdatePlayerAnimations()
    {
        myAnimator.SetFloat("HorizontalVel", Mathf.Abs(myMovement.currentVelocity_X));
        myAnimator.SetFloat("HorizontalInput", Mathf.Abs(Input.GetAxisRaw("Horizontal")));
        myAnimator.SetFloat("VerticalVel", MyRigidbody.velocity.y);
        myAnimator.SetBool("IsGrounded", myGroundCheck.isGrounded);
    }

    private void Die()
    {
        myAudio.PlayDeathSound();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    
    public void GetDamage(int damage)
    {
        myAudio.PlayDamagedSound();
        myHealth.SubtractPoints(damage);
        
        if (myHealth.CurrentPoints <= 0)
        {
            Die();
        }
    }

    private void OnGrounded()
    {
        myAudio.PlayGroundedSound();
    }
}
