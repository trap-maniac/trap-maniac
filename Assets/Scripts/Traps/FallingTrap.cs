﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingTrap : MonoBehaviour{
    [SerializeField] private Rigidbody2D _rigidbody2D;
    [SerializeField][Range(0, 10)] private float downForce;
        
    private void OnTriggerEnter2D(Collider2D other){
        if (other.gameObject.layer == LayerMask.NameToLayer("Player")){
            _rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
            _rigidbody2D.AddForce(new Vector2(0, -downForce * 1_000_000));
        }
    }

    void Start(){
    }

    void Update(){
    }
}