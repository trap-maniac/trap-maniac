﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawTrap : MonoBehaviour
{
    [Range(-50f, 50f)] public float rotatingSpeed = 10f;
    [Range(1f, 50f)] public float pushbackForce = 1f;
    public LayerMask affectedLayers;
    public bool isMoving = false;
    public Vector2 offset = Vector2.zero;
    [Range(1f, 50f)] public float movingSpeed = 1f;
    public float delay = 0f;
    public GameObject blood;

    private Vector2 basePosition;
    IEnumerator currentCoroutine;

    // Start is called before the first frame update
    void Start()
    {
        basePosition = transform.position;
        if(isMoving)
            StartCoroutine(Move());
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward * (-rotatingSpeed * Time.deltaTime * 1000));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (affectedLayers == (affectedLayers | (1 << collision.gameObject.layer)))
        {
            Instantiate(blood, collision.transform.position, Quaternion.identity);
            Vector3 dirrction = collision.transform.position - transform.position;

            var rb = collision.GetComponent<Rigidbody2D>();
            rb.velocity = Vector2.zero;
            rb.AddForce(dirrction.normalized * 500 * pushbackForce);
            
            collision.GetComponent<ICanBeDamaged>().GetDamage(1);

            PlayerMovement playerMovement;
            if (playerMovement = collision.GetComponent<PlayerMovement>())
            {
                playerMovement.ResetDeceleration();
                playerMovement.ResetAcceleration();
            }
        }
    }

    private IEnumerator Move()
    {
        Vector2 newPosition = basePosition + offset;
        Vector2 destination = newPosition;
        while (isMoving)
        {
            if ((Vector2)transform.position == basePosition)
                destination = newPosition;
            else
                destination = basePosition;
                
            while ((Vector2)transform.position != destination)
            {
                transform.position = Vector3.MoveTowards(transform.position, destination, movingSpeed * Time.deltaTime);
                yield return 0;
            }
            yield return new WaitForSeconds(delay);
        }           
    }
}
