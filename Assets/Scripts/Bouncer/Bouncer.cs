﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bouncer : MonoBehaviour
{
    [SerializeField] private float bounceForce = 10f;
    [SerializeField] private AudioSource mySound;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        Rigidbody2D rb;
        
        if (rb = other.GetComponent<Rigidbody2D>())
        {
            rb.velocity = new Vector2(rb.velocity.x, 0f);
            rb.AddForce(this.transform.up * bounceForce);
            mySound.Play();
        }
    }
}
