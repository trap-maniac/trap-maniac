﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "LevelData", menuName = "Levels/LevelsData")]
public class LevelsData : ScriptableObject {
    [SerializeField] public int lastPassedLevel;
    [SerializeField] private List<int> score = new List<int>();
    [SerializeField] private List<float> times = new List<float>();

    public void incrementLevel(int currentLevel) {
        if(currentLevel > lastPassedLevel) {
            lastPassedLevel = currentLevel;
        }
    }

    public bool isLevelAvailable(int level) {
        if(level > lastPassedLevel + 1) {
            return false;
        }
        return true;
    }

    public int getScoreOnLevel(int level) {
        return score[level];
    }

    public void setScoreOnLevel(int level, int sc) {
        if (score.Count - 1 > level) {
            score[level] = sc;
        } else if (score.Count - 1 == level) {
            score[level] = sc;
            score.Add(0);
        }
    }

    public void setTimeOnLevel(int level, float sc) {
        if (times.Count - 1 > level) {
            times[level] = sc;
        }
        else if (times.Count - 1 == level) {
            times[level] = sc;
            times.Add(0);
        }
    }

    public float getTimeOnLevel(int level) {
        return times[level];
    }

    public string getTimeOnLevelAsString(int level) {
        int minutes = Mathf.FloorToInt(times[level] / 60F);
        int seconds = Mathf.FloorToInt(times[level] - minutes * 60);
        var text = $"{minutes:00}:{seconds:00}";
        return text;
    }


}
