﻿using System;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    [Header("Dependencies")]
    [SerializeField] private PlayerController myPlayer;

    [Space]
    [Header("Settings")]
    [SerializeField] private int cameraSize;
    [SerializeField] private bool followPlayer;
    [SerializeField] private float cameraFollowSpeed;
    
    private Camera myCamera;
    
    private void Awake()
    {
        myCamera = GetComponent<Camera>();

        if (myPlayer == null)
        {
            myPlayer = FindObjectOfType<PlayerController>();
        }
    }

    private void Start()
    {
        myCamera.orthographicSize = cameraSize;
    }

    private void FixedUpdate()
    {
        if (followPlayer)
        {
            var yPos = Mathf.Lerp(transform.position.y, myPlayer.transform.position.y + 5f, cameraFollowSpeed * Time.deltaTime);
            transform.position = new Vector3(myPlayer.transform.position.x, yPos, -10f);
        }
    }
}
