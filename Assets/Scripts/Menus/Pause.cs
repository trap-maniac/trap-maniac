﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour{

    [SerializeField] private GameObject pausePanel;
    
    public static bool isPaused;

    public void OnDisable(){
        Time.timeScale = 1;
        isPaused = false;
    }

    public void PauseGame(){
            if (!isPaused){
                Time.timeScale = 0;
                pausePanel.SetActive(true);
            }
            else {
                Time.timeScale = 1;
                pausePanel.SetActive(false);
            }
            isPaused = !isPaused;
        }
    
    private void Update(){
        if (Input.GetKeyDown(KeyCode.Escape)){
            PauseGame();
        }
    }
    
    public void RestartLevel(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
