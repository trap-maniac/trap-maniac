﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour{
    private void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            LevelAudioManager.instance.PlayMenuMusic();
        }
    }

    public void LoadLevel(int levelIndex){
        SceneManager.LoadScene(levelIndex);
    }
    
    public void QuitGame(){
        Application.Quit();
    }
}
