﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class hud_script : MonoBehaviour
{
    [SerializeField] private Health playerHealth;
    
    public Image[] hearts;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI timeText;
    private float time;

    void Update()
    {
        scoreText.text = Score.score.ToString();
        time += Time.deltaTime;

        int minutes = Mathf.FloorToInt(time / 60F);
        int seconds = Mathf.FloorToInt(time - minutes * 60);
        timeText.text = $"{minutes:00}:{seconds:00}";
        Timer.currentTime = timeText.text;
        Timer.time = time;

        foreach (var heart in hearts)
        {
            heart.gameObject.SetActive(false);
        }

        for (int i = 0; i < playerHealth.CurrentPoints; i++)
        {
            hearts[i].gameObject.SetActive(true);
        }
    }
}
