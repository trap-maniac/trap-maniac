﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressManager : MonoBehaviour {
    [SerializeField] private LevelsData levelData;
    [SerializeField] private List<GameObject> levels = new List<GameObject>();
    [SerializeField] private List<LvlSummary> levelsSummary = new List<LvlSummary>();

    void Start() {
        int nextLevelIndex = 1;
        foreach(GameObject level in levels) {
            if(!levelData.isLevelAvailable(nextLevelIndex)) {
                levels[nextLevelIndex - 1].GetComponent<Button>().interactable = false;
                levelsSummary[nextLevelIndex - 1].gameObject.SetActive(false);
            } else {
                levelsSummary[nextLevelIndex - 1].gameObject.SetActive(true);
                levelsSummary[nextLevelIndex - 1].setScoreValue(levelData.getScoreOnLevel(nextLevelIndex - 1));
                levelsSummary[nextLevelIndex - 1].setTimeValue(levelData.getTimeOnLevelAsString(nextLevelIndex - 1));
            }
            nextLevelIndex++;
        }
    }
}
