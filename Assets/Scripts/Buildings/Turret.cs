﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Turret : MonoBehaviour {
    [SerializeField] private float attackRange;
    [SerializeField] private float timeBetweenShotsInSeconds;
    [SerializeField] private int projectileSpeed;
    [SerializeField] private bool trackPlayer;
    [SerializeField] private Transform firePointTransform;
    [SerializeField] private Transform ruffleTransform;
    [SerializeField] private GameObject turretProjectile;
    [SerializeField] private AudioSource shotAudioSource;

    private double lastShotTimestampInSeconds;
    private Transform playerTransform;

    void Awake() {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }
    
    // Update is called once per frame
    void Update() {
        if(trackPlayer && playerTransform) {
            this.updateRuffleDirection();
        }

        TimeSpan currentTime = (DateTime.UtcNow - new DateTime(1970, 1, 1));
        if(!this.passedTimeSinceLastShoot(currentTime)) {
            return;
        } else {
            this.shoot(currentTime);
        }
    }

    bool passedTimeSinceLastShoot(TimeSpan currentTime) {
        bool passedTimeSinceLastShoot = false;
        if(currentTime.TotalSeconds - lastShotTimestampInSeconds >= timeBetweenShotsInSeconds) {
            passedTimeSinceLastShoot = true;
        }
        return passedTimeSinceLastShoot;
    }

    void shoot(TimeSpan currentTime) {
        lastShotTimestampInSeconds = currentTime.TotalSeconds;
        TurretProjectile projectile = turretProjectile.GetComponent<TurretProjectile>();
        projectile.speed = projectileSpeed;
        projectile.lifetime = attackRange;
        Instantiate(projectile, firePointTransform.position, firePointTransform.rotation);
        shotAudioSource.Play();
    }

    void updateRuffleDirection() {
        Vector3 direction = playerTransform.position - ruffleTransform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        ruffleTransform.rotation = Quaternion.Slerp(ruffleTransform.rotation, rotation, 10 * Time.deltaTime);
    }
}