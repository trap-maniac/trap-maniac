﻿
using UnityEngine;

public class HealthCollectable : Collectable {
    [SerializeField] [Range(1, 10)] private int amountOfHp;
    public override void OnPickUpEffect(GameObject collector) {
        Health health;
        if (health = collector.GetComponent<Health>()) {
            health.AddPoints(1);
        }
    }
}
