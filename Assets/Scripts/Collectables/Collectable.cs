﻿using UnityEngine;

public class Collectable : MonoBehaviour {

    public LayerMask affectedLayers;
    public AudioSource mySound;
    
    public virtual void OnPickUpEffect(GameObject collector) {
    }

    void OnTriggerEnter2D(Collider2D collider2D)
    {
        if (affectedLayers == (affectedLayers | (1 << collider2D.gameObject.layer))) {
            OnPickUpEffect(collider2D.gameObject);
            mySound.Play();
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<CircleCollider2D>().enabled = false;
        }
    }
}
