﻿using UnityEngine;

public class ScoreCollectable: Collectable {
    [SerializeField] [Range(1, 500)] private int scoreBonus;

    public override void OnPickUpEffect(GameObject collector) {
        Score.AddScore(scoreBonus);
    }
}
