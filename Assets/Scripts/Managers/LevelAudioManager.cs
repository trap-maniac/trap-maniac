﻿using UnityEngine;

public class LevelAudioManager : MonoBehaviour
{
    public static LevelAudioManager instance;

    public AudioClip menuSoundtrack;
    public AudioClip gameplaySoundtrack;
    
    private AudioSource myAudioSource;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
 
        instance = this;
        DontDestroyOnLoad( this.gameObject );

        myAudioSource = GetComponent<AudioSource>();
    }

    public void PlayMenuMusic()
    {
        if (myAudioSource.clip != menuSoundtrack)
        {
            myAudioSource.clip = menuSoundtrack;
            myAudioSource.Play();   
        }
    }

    public void PlayGameplayMusic()
    {
        if (myAudioSource.clip != gameplaySoundtrack)
        {
            myAudioSource.clip = gameplaySoundtrack;
            myAudioSource.Play();   
        }
    }
}
