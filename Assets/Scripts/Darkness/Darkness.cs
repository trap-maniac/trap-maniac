﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Darkness : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        other.GetComponent<ICanBeDamaged>().GetDamage(10000);

        if (other.GetComponent<PlayerController>())
        {
            //temp
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
