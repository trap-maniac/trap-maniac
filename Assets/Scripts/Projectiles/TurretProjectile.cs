﻿using UnityEngine;

public class TurretProjectile : MonoBehaviour {
    public int speed = 50;
    public float lifetime = 10f;
    public int damage = 1;
    public Rigidbody2D rb;
    [Range(1f, 50f)] public float pushbackForce = 1f;
    public GameObject blood;

    // Start is called before the first frame update
    void Start() {
        rb.velocity = transform.right * speed;
    }

    // Update is called once per frame
    void Update() {
        lifetime -= Time.deltaTime;
        if (lifetime <= 0f) {
            // we have ran out of life
            Destroy(gameObject); // kill me
        }
    }

    // Triggers on collide with other object
    void OnTriggerEnter2D(Collider2D collision) {
        ICanBeDamaged damagedObj = collision.GetComponent<ICanBeDamaged>();
        if(damagedObj != null) {
            Instantiate(blood, collision.transform.position, Quaternion.identity);
            Vector3 dirrction = collision.transform.position - transform.position;

            var rb = collision.GetComponent<Rigidbody2D>();
            rb.velocity = Vector2.zero;
            rb.AddForce(dirrction.normalized * 500 * pushbackForce);

            PlayerMovement playerMovement;
            if (playerMovement = collision.GetComponent<PlayerMovement>())
            {
                playerMovement.ResetDeceleration();
                playerMovement.ResetAcceleration();
            }
            damagedObj.GetDamage(damage);
        }
        Destroy(gameObject); // kill me
    }
}
