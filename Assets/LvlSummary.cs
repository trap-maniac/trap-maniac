﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LvlSummary : MonoBehaviour {
    [SerializeField] private TextMeshProUGUI scoreValue;
    [SerializeField] private TextMeshProUGUI timeCountValue;

    public void setScoreValue(int score) {
        scoreValue.text = score.ToString();
    }

    public void setTimeValue(string time) {
        timeCountValue.text = time;
    }
}
